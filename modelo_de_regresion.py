Luciana Alexandra Fernandez Lopez
codigo: 2020700992

# Crear el modelo de regresión logística
model = LogisticRegression()

# Entrenar el modelo
model.fit(X_train, y_train)
