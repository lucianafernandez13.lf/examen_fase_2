Luciana Alexandra Fernandez Lopez
codigo: 2020700992

# Cargar los datos CSV
data = pd.read_csv('datos.csv', delimiter=';')

# Convertir respuestas a valores numéricos
data.replace({'En desacuerdo totalmente': 0, 'En desacuerdo': 1, 'Irrelevante': 2, 'De acuerdo': 3, 'De acuerdo totalmente': 4}, inplace=True)
