Luciana Alexandra Fernandez Lopez
codigo: 2020700992

# División de datos en características y variable objetivo
X = data[['Q0', 'Q1', 'Q2', 'Q3', 'Q4']]
y = data['Divorcio']

# División de datos en conjuntos de entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
